# 👻 Yo!

```yaml
apiVersion: v21
kind: Human
metadata:
  name: Noppakorn Kaewsalabnil
spec:
  purpose: Learn and become a DevOps
  locations:
    - type: University
      where: King Mongkut's Institute of Technology Ladkrabang (KMITL)
    - type: Homepage
      where: www.pungrumpy.com
    - type: Country
      where: Thailand
  favorites:
    animal: 🐈 🐕 🐋
    emoji: 👻
```

<br />

## ⌨️ Skills

[![Python](https://img.shields.io/badge/Python-14354c?style=for-the-badge&logo=python&logoColor=ffdd54)](https://www.python.org/)
[![CSS](https://img.shields.io/badge/CSS-264de4?style=for-the-badge&logo=css3&logoColor=white)](https://github.com/PunGrumpy)
[![HTML](https://img.shields.io/badge/HTML-e34c26?style=for-the-badge&logo=html5&logoColor=white)](https://github.com/PunGrumpy)
[![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)](https://www.javascript.com/)
[![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)](https://www.typescriptlang.org)
[![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white)](https://go.dev/)
[![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)](https://www.docker.com/)
[![Kubernetes](https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white)](https://kubernetes.io/)
[![Helm](https://img.shields.io/badge/helm-%23ffffff.svg?style=for-the-badge&logo=helm&logoColor=%230f1689)](https://helm.sh/)
[![Apache Hadoop](https://img.shields.io/badge/Apache%20Hadoop-66CCFF?style=for-the-badge&logo=apachehadoop&logoColor=black)](https://hadoop.apache.org/)
[![GitHub Actions](https://img.shields.io/badge/github%20actions-%232671E5.svg?style=for-the-badge&logo=githubactions&logoColor=white)](https://github.com/features/actions)
[![TravisCI](https://img.shields.io/badge/travis%20ci-%232B2F33.svg?style=for-the-badge&logo=travis&logoColor=white)](https://www.travis-ci.com/)
[![ArgoCD](https://img.shields.io/badge/argo%20cd-%23fffeff.svg?style=for-the-badge&logo=argo&logoColor=%23ee6936)](https://argo-cd.readthedocs.io/)
[![Ansible](https://img.shields.io/badge/ansible-%231A1918.svg?style=for-the-badge&logo=ansible&logoColor=white)](https://www.ansible.com/)
[![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)](https://www.terraform.io/)
[![TailwindCSS](https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white)](https://tailwindcss.com/)
[![MongoDB](https://img.shields.io/badge/MongoDB-3FA037?style=for-the-badge&logo=mongodb&logoColor=white)](https://www.mongodb.com/)
[![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)](https://www.postgresql.org/)
[![ExpressJS](https://img.shields.io/badge/express.js-fff.svg?style=for-the-badge&logo=express&logoColor=000)](https://expressjs.com/)
[![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)](https://reactjs.org/)
[![Next JS](https://img.shields.io/badge/Next-black?style=for-the-badge&logo=next.js&logoColor=white)](https://nextjs.org/)
[![Prisma](https://img.shields.io/badge/Prisma-3982CE?style=for-the-badge&logo=Prisma&logoColor=white)](https://www.prisma.io/)
[![Cypress](https://img.shields.io/badge/-cypress-%23fff?style=for-the-badge&logo=cypress&logoColor=058a5e)](https://www.cypress.io/)
[![Blender](https://img.shields.io/badge/blender-EA7600.svg?style=for-the-badge&logo=blender&logoColor=white)](https://www.blender.org/)
[![Adobe Lightroom](https://img.shields.io/badge/Adobe%20Lightroom-30a8ff?style=for-the-badge&logo=Adobe%20Lightroom&logoColor=001e36)](https://lightroom.adobe.com)
[![Figma](https://img.shields.io/badge/figma-%2323231E.svg?style=for-the-badge&logo=figma&logoColor=white)](https://www.figma.com/)
  
</div>

<div>

## 🧑‍💻 IDE & Hosting

[![Replit](https://img.shields.io/badge/replit-000001?style=for-the-badge&logo=replit&logoColor=white)](https://replit.com/~)
[![Vscode](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)](https://code.visualstudio.com/)
[![NeoVim](https://img.shields.io/badge/NeoVim-%2311AB00.svg?&style=for-the-badge&logo=neovim&logoColor=white)](https://neovimcraft.com/)
[![CodeSandBox](https://img.shields.io/badge/Codesandbox-000000?style=for-the-badge&logo=CodeSandbox&logoColor=white)](https://codesandbox.io/)
[![Gitpod](https://img.shields.io/badge/gitpod-f06611.svg?style=for-the-badge&logo=gitpod&logoColor=white)](https://gitpod.io/)
[![DigitalOcean](https://img.shields.io/badge/DigitalOcean-%230167ff.svg?style=for-the-badge&logo=digitalOcean&logoColor=white)](https://www.digitalocean.com/?refcode=a33b42e7f83b&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)
[![Netlify](https://img.shields.io/badge/netlify-%23000000.svg?style=for-the-badge&logo=netlify&logoColor=#00C7B7)](https://www.netlify.com/)
[![Vercel](https://img.shields.io/badge/Vercel-000000?style=for-the-badge&logo=vercel&logoColor=white)](https://vercel.com/)

</div>

<div>

## 💻 OS

[![Windows11Pro](https://img.shields.io/badge/Windows%2011-%230079d5.svg?style=for-the-badge&logo=Windows%2011&logoColor=white)](https://www.microsoft.com/th-th/windows?r=1)
[![Kali Linux](https://img.shields.io/badge/Kali-2675fc?style=for-the-badge&logo=kalilinux&logoColor=black)](https://www.kali.org/)
[![Kali Purple Linux](https://img.shields.io/badge/Kali-ba67d3?style=for-the-badge&logo=kalilinux&logoColor=white)](https://www.kali.org/)
[![Ubuntu on WSL2](https://img.shields.io/badge/WSL-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)](https://learn.microsoft.com/en-us/windows/wsl/install)

</div>

<div>

## 🤝 Connect with Me

[![Discord](https://img.shields.io/badge/Discord-5865F2?style=for-the-badge&logo=discord&logoColor=white)](https://discordapp.com/users/353899973252874260)
[![Spotify](https://img.shields.io/badge/Spotify-1ED760?&style=for-the-badge&logo=spotify&logoColor=white)](https://open.spotify.com/user/t504rlopu4cif7rn4bpbopqde)

</div>

<div>

## 🎁 Support

[![Paypal](https://img.shields.io/badge/PayPal-00457C?style=for-the-badge&logo=paypal&logoColor=white)](nopakron41310@gmail.com)
[![ko-fi](https://img.shields.io/badge/Ko--fi-F16061?style=for-the-badge&logo=ko-fi&logoColor=white)](https://ko-fi.com/pungrumpy)

</div>

<div>

## 🌐 Holopin Badges

[![@pungrumpy's Holopin board](https://holopin.me/pungrumpy)](https://holopin.io/@pungrumpy)

</div>

##

</p>

<div>
  <a href="https://github.com/PunGrumpy" style="text-decoration:none">
    <img src="https://komarev.com/ghpvc/?username=PunGrumpy">
</a>
<a href="https://github.com/PunGrumpy?tab=followers"><img src="https://img.shields.io/github/followers/PunGrumpy?label=Followers&style=social" alt="GitHub Badge"></a>
</div>
